#!/bin/bash

#---------------------------------------------------Microbenchmarks-Streaming-Writes-and-Reads--------------------------------------------------#
echo -e "\nMicrobenchmarks-Streaming-Writes-and-Reads\n"

cd Microbenchmarks-Streaming-Writes-and-Reads

echo deadline > /sys/block/sda/queue/scheduler
cat /sys/block/sda/queue/scheduler
echo -e "\nDEADLINE\n"

./script.sh &
sleep 7
./script2.sh 
sleep 7
./script2.sh 
sleep 7
./script2.sh 
wait
echo -e "\n"


echo noop > /sys/block/sda/queue/scheduler
cat /sys/block/sda/queue/scheduler
echo -e "\nNOOP\n"

./script.sh &
sleep 7
./script2.sh 
sleep 7
./script2.sh 
sleep 7
./script2.sh 
wait
echo -e "\n"

echo cfq > /sys/block/sda/queue/scheduler
cat /sys/block/sda/queue/scheduler
echo -e "\nCFQ\n"

./script.sh &
sleep 7
./script2.sh 
sleep 7
./script2.sh 
sleep 7
./script2.sh 
wait
echo -e "\n"

rm file_test

#--------------------------------------------------------------Microbenchmarks-Streaming-and-Chunk-Reads--------------------------------------#
echo -e "\nMicrobenchmarks-Streaming-and-Chunk-Reads\n"
cd ..
cd Microbenchmarks-Streaming-and-Chunk-Reads
dd if=/dev/zero of=file_test count=16376 bs=1M
dd if=/dev/zero of=file_test-2 count=16376 bs=1M

echo deadline > /sys/block/sda/queue/scheduler
cat /sys/block/sda/queue/scheduler
echo -e "\nDEADLINE\n"

sleep 2

./script.sh &
sleep 2
./script2.sh 
sleep 2
./script2.sh 
sleep 2
./script2.sh 
wait
echo -e "\n"


echo noop > /sys/block/sda/queue/scheduler
cat /sys/block/sda/queue/scheduler
echo -e "\nNOOP\n"

sleep 2

./script.sh &
sleep 2
./script2.sh 
sleep 2
./script2.sh 
sleep 2
./script2.sh 
wait
echo -e "\n"

echo cfq > /sys/block/sda/queue/scheduler
cat /sys/block/sda/queue/scheduler
echo -e "\nCFQ\n"

sleep 2

./script.sh &
sleep 2
./script2.sh 
sleep 2
./script2.sh 
sleep 2
./script2.sh 
wait
echo -e "\n"

rm file_test
rm file_test-2

#--------------------------------------------------------------Microbenchmarks-Chunk-Reads-----------------------------------------------#
echo -e "\nMicrobenchmarks-Chunk-Reads\n"
cd ..
cd Microbenchmarks-Chunk-Reads

dd if=/dev/zero of=file_test count=16376 bs=1M
dd if=/dev/zero of=file_test-2 count=16376 bs=1M

echo deadline > /sys/block/sda/queue/scheduler
cat /sys/block/sda/queue/scheduler
echo -e "\nDEADLINE\n"

sleep 2
echo -e "\nOne Instance\n"
./script.sh 
sleep 2

echo -e "\nTwo Instance\n"
./script.sh &
./script.sh 
wait
echo -e "\n"


echo noop > /sys/block/sda/queue/scheduler
cat /sys/block/sda/queue/scheduler
echo -e "\nNOOP\n"

sleep 2
echo -e "\nOne Instance\n"
./script.sh 
sleep 2

echo -e "\nTwo Instance\n"
./script.sh &
./script.sh 
wait
echo -e "\n"

echo cfq > /sys/block/sda/queue/scheduler
cat /sys/block/sda/queue/scheduler
echo -e "\nCFQ\n"

sleep 2
echo -e "\nOne Instance\n"
./script.sh 
sleep 2

echo -e "\nTwo Instance\n"
./script.sh &
./script.sh 
wait
echo -e "\n"

rm file_test
rm file_test-2
cd ..

